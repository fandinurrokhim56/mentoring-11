<?php
class M_mahasiswa extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_mahasiswa()
  {
    $query = $this->db->get('mahasiswa');
    return $query->result_array();
  }

  public function tambah_mahasiswa($data)
  {
    return $this->db->insert('mahasiswa', $data);
  }

  public function get_mahasiswa_by_id($id)
  {
    $query = $this->db->get_where('mahasiswa', array('id' => $id));
    return $query->row_array();
  }

  public function update_mahasiswa($data, $id)
  {
    $this->db->where('id', $id);
    return $this->db->update('mahasiswa', $data);
  }

  public function hapus_mahasiswa($id)
  {
    $this->db->where('id', $id);
    return $this->db->delete('mahasiswa');
  }
}
