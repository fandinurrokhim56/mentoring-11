<?php
class Mahasiswa extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->helper('url');
    $this->load->model('M_mahasiswa');
    $this->load->model('M_hobi');
  }

  public function index()
  {
    $data['mahasiswa'] = $this->M_mahasiswa->get_all_mahasiswa();
    $this->load->view('mahasiswa/index', $data);
  }

  public function tambah()
  {
    $data['hobi'] = $this->M_hobi->get_all_hobi();
    $this->load->view('mahasiswa/tambah', $data);
  }

  public function simpan()
  {
    $this->db->trans_start();
    $this->db->insert('mahasiswa', [
      'nim' => $this->input->post('nim'),
      'nama' => $this->input->post('nama'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'alamat' => $this->input->post('alamat'),
    ]);
    
    $hobi = [];
    foreach ($this->input->post('id_hobi[]') as $row) {
      $hobi[] = [
        'id_mahasiswa' => $this->db->insert_id(),
        'id_hobi' => $row,
      ];
    }

    $this->db->insert_batch('mahasiswa_hobi', $hobi);
    $this->db->trans_complete();
    redirect(base_url('mahasiswa'));
  }

  public function edit($id)
  {
    $data['hobi'] = $this->M_hobi->get_all_hobi();
    $data['mahasiswa'] = $this->M_mahasiswa->get_mahasiswa_by_id($id);
    $this->load->view('mahasiswa/edit', $data);
  }

  public function update($id)
  {
    $this->db->trans_start();
    $this->db->where('id', $id);
    $this->db->update('mahasiswa', [
      'nim' => $this->input->post('nim'),
      'nama' => $this->input->post('nama'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'alamat' => $this->input->post('alamat'),
    ]);

    if ($this->input->post('id_hobi[]')) {
      $this->db->where('id_mahasiswa', $id);
      $this->db->delete('mahasiswa_hobi');
      $hobi = [];
      foreach ($this->input->post('id_hobi[]') as $row) {
        $hobi[] = [
          'id_mahasiswa' => $id,
          'id_hobi' => $row,
        ];
      }
      $this->db->insert_batch('mahasiswa_hobi', $hobi);
    }

    $this->db->trans_complete();
    redirect(base_url('mahasiswa'));
  }

  public function hapus($id)
  {
    $this->db->trans_start();
    $this->db->where('id_mahasiswa', $id);
    $this->db->delete('mahasiswa_hobi');
    $this->db->where('id', $id);
    $this->db->delete('mahasiswa');
    $this->db->trans_complete();
    redirect(base_url('mahasiswa'));
  }
}