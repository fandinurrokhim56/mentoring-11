<!DOCTYPE html>
<html>

<head>
  <title>Daftar Mahasiswa</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<body>
  <div class="container">
    <h2 class="mt-4">Daftar Mahasiswa</h2>
    <a href="<?= base_url('mahasiswa/tambah'); ?>" class="btn btn-primary mb-2">Tambah Mahasiswa</a>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>NIM</th>
          <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Alamat</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($mahasiswa as $row): ?>
          <tr>
            <td><?= $row['id']; ?></td>
            <td><?= $row['nim']; ?></td>
            <td><?= $row['nama']; ?></td>
            <td><?= $row['jenis_kelamin']; ?></td>
            <td><?= $row['alamat']; ?></td>
            <td>
              <a href="<?= base_url('mahasiswa/edit/' . $row['id']); ?>" class="btn btn-warning btn-sm">Edit</a>
              <a href="<?= base_url('mahasiswa/hapus/' . $row['id']); ?>" class="btn btn-danger btn-sm">Hapus</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</body>

</html>