<!DOCTYPE html>
<html>

<head>
  <title>Tambah Mahasiswa</title>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
  <div class="container bg-light mt-5 border border-primary rounded-3 shadow-sm w-50 p-5 mx-auto">
    <p class="fw-bold fs-5 ">Tambah Mahasiswa</p>
    <form method="post" action="<?= base_url('mahasiswa/simpan'); ?>" class="mt-3">
      <div class="mb-3">
        <label for="nim" class="form-label">NIM:</label>
        <input type="text" class="form-control" id="nim" name="nim" required>
      </div>

      <div class="mb-3">
        <label for="nama" class="form-label">Nama:</label>
        <input type="text" class="form-control" id="nama" name="nama" required>
      </div>

      <div class="mb-3">
        <label for="jenis_kelamin" class="form-label">Jenis Kelamin:</label>
        <select class="form-select" id="jenis_kelamin" name="jenis_kelamin" required>
          <option value="">Pilih Jenis Kelamin</option>
          <option value="L">Laki-laki</option>
          <option value="P">Perempuan</option>
        </select>
      </div>

      <div class="mb-3">
        <label for="id_hobi" class="form-label">Hobi:</label>
        <select class="form-select select2" id="id_hobi" name="id_hobi[]" multiple required>
          <option value="">Pilih Hobi</option>

          <?php foreach ($hobi as $row): ?>
            <option value="<?= $row['id']; ?>">
              <?= $row['hobi']; ?>
            </option>
          <?php endforeach; ?>

        </select>
      </div>

      <div class="mb-3">
        <label for="alamat" class="form-label">Alamat:</label>
        <textarea class="form-control" id="alamat" name="alamat" required></textarea>
      </div>

      <div class="d-flex justify-content-end align-items-center">
        <a href="<?= base_url('mahasiswa'); ?>" class="btn btn-light shadow-sm me-2">Cancel</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>

  <script src="https://code.jquery.com/jquery-3.7.1.min.js"
    integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
    $(document).ready(function () {
      $('.select2').select2();
    });
  </script>
</body>

</html>